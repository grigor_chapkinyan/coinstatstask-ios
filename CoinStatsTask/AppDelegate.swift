//
//  AppDelegate.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 15.07.21.
//

import UIKit
import RealmSwift
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: Properties
    
    var window: UIWindow?
    
    // MARK: App Life Cycle
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        configureRealmSchemaVersion()
        setupAllServicesAndManagers()
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
                        [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        configureKeyboard()
        print(NSLocalizedString("English", comment: ""))
        
        return true
    }
}

// MARK: - Helpers

extension AppDelegate {
    private func configureRealmSchemaVersion() {
        let config = Realm.Configuration(schemaVersion: UInt64(Constants.realmSchemaVersion))
        Realm.Configuration.defaultConfiguration = config
    }

    private func configureKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    private func setupAllServicesAndManagers() {
        VisibilityService.setup()
        DBService.setup()
    }
}
