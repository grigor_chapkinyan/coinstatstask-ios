//
//  UserDataManager.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 01.03.21.
//

import Foundation
import RxCocoa
import RxSwift
import RealmSwift

class UserDataManager: DataManager {
    // MARK: Output
    
    public let storedUserIds = BehaviorRelay<[String]>(value: [])
    
    // MARK: Init
    
    static var shared: UserDataManager = UserDataManager()
    private override init() {
        super.init()
        
        helperQueue.async { [weak self] in
            self?.setupIntialBindings()
        }
    }
    
    // MARK: Properties
    
    private lazy var helperQueue = DispatchQueue.global(qos: .default)
    private lazy var helperScheduler = SerialDispatchQueueScheduler(queue: helperQueue, internalSerialQueueName: "UserDataManager")
    private let bag = DisposeBag()
    
    // MARK: Methods
    
    func storeUser(user: User) -> Observable<Void> {
        guard  !user.isInvalidated else {
            return Observable.error(DataManager.Error.objectAbsents)
        }
        
        return updateUsers(with: [user], mustCleanOddObjects: false)
    }
    
    func getAndStoreUser(with id: String) -> Observable<Void> {
        return getUsers()
            .map({ return $0.filter({ return ((!$0.isInvalidated) && ($0._id == id)) }).first })
            .subscribe(on: helperScheduler)
            .flatMap { [weak self] (userOptional) -> Observable<Void> in
                if let user = userOptional {
                    return self?.updateUsers(with: [user], mustCleanOddObjects: false) ?? Observable.just(())
                }
                else {
                    return Observable.error(DataManager.Error.objectAbsents)
                }
            }
    }
    
    func updateStoredInstances() -> Observable<Void> {
        return getUsers()
            .flatMap { [weak self] in return self?.updateStoredUsers(from: $0) ?? Observable.just(()) }
    }
    
    func removeUsers(with ids: [String]) -> Observable<Void> {
        let predicate = NSPredicate(format: "_id IN %@", ids)
        
        return removeUsers(with: predicate)
    }
    
    func getUsers(resultCount: Int = 50, pageNumber: Int = 1) -> Observable<[User]> {
        let params: [String : String] = [
            Constants.HttpBodyKeys.results.rawValue : String(resultCount),
            Constants.HttpBodyKeys.page.rawValue : String(pageNumber),
        ]
        
        return NetworkService
            .shared
            .sendHttpRequest(urlString: Constants.APIEndpoint.baseUrl() + Constants.APIEndpoint.default.rawValue, httpMethod: .get, queryParametrs: params)
            .subscribe(on: helperScheduler)
            .flatMap { [weak self] (networkRessponse) -> Observable<[User]> in
                return self?.usersFrom(networkResponse: networkRessponse) ?? Observable.just([])
            }
    }
        
    // MARK: Helpers
    
    private func setupIntialBindings() {
        Realm
            .objectsNotifier(type: .user, scheduler: helperScheduler)
            .compactMap { (objects) in
                return objects?
                    .filter({ !$0.isInvalidated })
                    .compactMap{ $0 as? User }
                    .sorted(by: {
                        return $0.id < $1.id
                    })
            }
            .map { (users) in
                return users.compactMap{ $0._id }
            }
            .bind(to: storedUserIds)
            .disposed(by: bag)
    }

    private func usersFrom(networkResponse: NetworkResponse) -> Observable<[User]> {
        guard   let dict = dictionaryFrom(networkResponse: networkResponse),
                let usersToReturn = usersFrom(dict: dict) else { return Observable.error(Error.decodeError) }
        
        return Observable.just(usersToReturn)
    }
    
    private func usersFrom(dict: [String : Any]) -> [User]? {
        guard let dictArray = dict[Constants.HttpBodyKeys.results.rawValue] as? [[String : Any]] else {
            return nil
        }
        
        let usersToReturn = dictArray.compactMap({ User(from: $0) })
        
        return usersToReturn
    }
    
    private func updateStoredUsers(from newInstances: [User]) -> Observable<Void> {
        let usersToUpdate = storedUsers(from: newInstances)
        
        return updateUsers(with: usersToUpdate, mustCleanOddObjects: false)
    }
    
    private func storedUsers(from remoteUsers: [User]) -> [User] {
        let usersToReturn: [User] = remoteUsers
            .filter({ (!$0.isInvalidated && storedUserIds.value.contains($0.id)) })
        
        return usersToReturn
    }
    
    private func updateUsers(with users: [User], mustCleanOddObjects: Bool) -> Observable<Void> {
        let usersToUpdate: RealmObjectWithIdArray = (User.self,users)
        let objectsToUpdate: [RealmObjectWithIdArray] = [usersToUpdate]
        let observableToReturn = updateDBWith(allTypeObjects: objectsToUpdate,mustCleanOddObjects: mustCleanOddObjects)
        
        return observableToReturn
    }
    
    private func removeUsers(with predicate: NSPredicate) -> Observable<Void> {
        return DBService.shared.remove(baseType: User.self, predicate: predicate)
    }
}
