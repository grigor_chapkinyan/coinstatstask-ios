//
//  DataManager.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 10.05.21.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift

class DataManager {
    // MARK: Nested Types
    
    enum Error: Swift.Error {
        case decodeError
        case tokenError
        case noInternetDestructiveAction
        case noInternetAddAction
        case noInternetEditAction
        case selfDestroyed
        case incompleteInputInfo
        case objectAbsents
    }
    
    enum InstanceType {
        case remote
        case local
    }
    
    // MARK: Helpers
    
    func handleErrorForEditCall(initialError: Swift.Error) -> Swift.Error {
        guard let httpRequestError = initialError as? HttpRequestError,
              httpRequestError == .noInternetConnection else {
            return initialError
        }
        
        return Error.noInternetEditAction
    }
    
    func handleErrorForAddCall(initialError: Swift.Error) -> Swift.Error {
        guard let httpRequestError = initialError as? HttpRequestError,
              httpRequestError == .noInternetConnection else {
            return initialError
        }
        
        return Error.noInternetAddAction
    }
    
    func handleErrorForDestructiveCall(initialError: Swift.Error) -> Swift.Error {
        guard let httpRequestError = initialError as? HttpRequestError,
              httpRequestError == .noInternetConnection else {
            return initialError
        }
        
        return Error.noInternetDestructiveAction
    }
    
    func updateDBWith(allTypeObjects: [RealmObjectWithIdArray], mustCleanOddObjects: Bool = true) -> Observable<Void> {
        let updateObservables = allTypeObjects.map { return updateDBWith(customTypeObjects: $0.1,baseType: $0.0, mustCleanOddObjects: mustCleanOddObjects) }
        let finalObservable = Observable
            .zip(updateObservables)
            .map { (_) in return () }
        
        return finalObservable
    }
    
    func updateDBWith(customTypeObjects: [RealmObjectWithId],baseType: Object.Type = RealmObjectWithId.self,mustCleanOddObjects: Bool = true) -> Observable<Void> {
        return DBService
            .shared
            .updateWith(newObjects: customTypeObjects,baseType: baseType,mustCleanOddObjects: mustCleanOddObjects)
    }
    
    func dictionaryFrom(networkResponse: NetworkResponse) -> [String : Any]? {
        guard   let data = networkResponse.data,
                let dict = try? JSONSerialization.jsonObject(with: data) as? [String : Any] else { return nil }
        
        return dict
    }
}
