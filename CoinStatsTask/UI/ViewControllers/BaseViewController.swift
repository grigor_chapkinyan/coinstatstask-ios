//
//  BaseViewController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 16.07.21.
//

import UIKit

class BaseViewController: UIViewController {
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupAnimatingActivityImageView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        configureSubviewsSequence()
    }

    // MARK: Properties
    
    private var activityIndicatorView: UIActivityIndicatorView?
    var isLoading: Bool {
        set {
            configureLoading(isEnabled: newValue)
        }
        get {
            return !view.isUserInteractionEnabled
        }
    }
    
    // MARK: Helpers
    
    private func setupAnimatingActivityImageView() {
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.stopAnimating()
                
        view.addSubview(activityIndicatorView)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: view!, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: activityIndicatorView, attribute: .centerY, relatedBy: .equal, toItem: view!, attribute: .centerY, multiplier: 1, constant: 0)
        ])
        
        self.activityIndicatorView = activityIndicatorView
    }
    
    private func configureSubviewsSequence() {
        if let activityIndicatorView = activityIndicatorView {
            view.bringSubviewToFront(activityIndicatorView)
        }
    }
    
    private func configureLoading(isEnabled: Bool) {
        view.isUserInteractionEnabled = !isEnabled
        
        isEnabled ? activityIndicatorView?.startAnimating() : activityIndicatorView?.stopAnimating()
    }
}
