//
//  CircleImageView.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 26.02.21.
//

import UIKit

class CircleImageView: UIImageView {
    // MARK: View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        changeLayerToCircle()
        contentMode = .scaleAspectFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init() {
        self.init(frame: .zero)
        
        changeLayerToCircle()
        contentMode = .scaleAspectFill
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    override func layoutSubviews() {
        changeLayerToCircle()
        configureBorders()
    }
    
    // MARK: IBInspectables
    
    @IBInspectable
    open var borderWidthMultiply: Float = .zero {
        didSet {
            configureBorders()
        }
    }
    
    @IBInspectable
    open var borderColor: UIColor = .clear {
        didSet {
            configureBorders()
        }
    }
    
    // MARK: Helpers
    
    private func changeLayerToCircle() {
        layer.masksToBounds = false
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }
    
    private func configureBorders() {
        layer.borderWidth = (bounds.width * CGFloat(borderWidthMultiply))
        layer.borderColor = borderColor.cgColor
    }
}
