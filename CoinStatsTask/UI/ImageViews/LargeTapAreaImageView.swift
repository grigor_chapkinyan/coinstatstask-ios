//
//  LargeTapAreaImageView.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 30.06.21.
//

import UIKit

@IBDesignable
class LargeTapAreaImageView: UIImageView {
    // MARK: IBInspectables
    
    @IBInspectable var margin:CGFloat = 0

    // MARK: View Life Cycle
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let area = self.bounds.insetBy(dx: -margin, dy: -margin)
        
        return area.contains(point)
    }
}
