//
//  UserDetailInfoViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift
import CoreLocation

// MARK: - UserListViewModel

struct UserDetailInfoViewModel {
    // MARK: Input
    
    public let emailSendBtnTap = PublishRelay<Void>()
    public let phoneBtnTap = PublishRelay<Void>()

    // MARK: Output

    public let openMailAppWithStr = PublishRelay<String?>()
    public let openPhoneAppWithPhoneNmb = PublishRelay<String?>()
    public let imageUrl = BehaviorRelay<URL?>(value: nil)
    public let age = BehaviorRelay<String?>(value: nil)
    public let gender = BehaviorRelay<String?>(value: nil)
    public let email = BehaviorRelay<String?>(value: nil)
    public let phoneNumber = BehaviorRelay<String?>(value: nil)
    public let locationDescription = BehaviorRelay<String?>(value: nil)
    public let location = BehaviorRelay<CLLocationCoordinate2D?>(value: nil)    
    
    // MARK: Init
    
    init?(user: User) {
        guard  !user.isInvalidated else {
            return nil
        }
        
        self.userId = user._id
        
        setupIntialBindings()
        configureData(for: user)
    }
    
    // MARK: Properties
    
    private let userId: String
    private let bag = DisposeBag()
    
    // MARK: Helpers
    
    private func setupIntialBindings() {
        phoneBtnTap
            .map({ phoneNumber.value })
            .bind(to: openPhoneAppWithPhoneNmb)
            .disposed(by: bag)
        
        emailSendBtnTap
            .map({ email.value })
            .bind(to: openMailAppWithStr)
            .disposed(by: bag)
    }
    
    private func configureData(for user: User) {
        let imageUrl = URL(string: user.largeImage)
        let age = String(user.age)
        let gender = user.genderStr
        let email = user.email
        let phoneNumber = user.phone
        let locationDescription = user.country + ", " + user.state + ", " + user.city + ", " + user.streetName + ", " + String(user.streetNumber)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: user.coordLat, longitude: user.coordLong)
        
        self.imageUrl.accept(imageUrl)
        self.age.accept(age)
        self.gender.accept(gender)
        self.email.accept(email)
        self.phoneNumber.accept(phoneNumber)
        self.locationDescription.accept(locationDescription)
        self.location.accept(location)
    }
}
