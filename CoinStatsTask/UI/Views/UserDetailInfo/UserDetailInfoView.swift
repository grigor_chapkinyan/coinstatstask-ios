//
//  UserDetailInfoView.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

class UserDetailInfoView: UIView {
    static let nibFileName = "UserDetailInfoView"
    
    // MARK: Outlets
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var agePlaceholderLabel: UILabel!
    @IBOutlet private weak var ageValueLabel: UILabel!
    @IBOutlet private weak var genderPlaceholderLabel: UILabel!
    @IBOutlet private weak var genderValueLabel: UILabel!
    @IBOutlet private weak var emailPlaceholderLabel: UILabel!
    @IBOutlet private weak var emailValueLabel: UILabel!
    @IBOutlet private weak var phonePlaceholderLabel: UILabel!
    @IBOutlet private weak var phoneValueLabel: UILabel!
    @IBOutlet private weak var addressPlaceholderLabel: UILabel!
    @IBOutlet private weak var addressValueLabel: UILabel!
    @IBOutlet private weak var emailSendButton: UIButton!
    @IBOutlet private weak var phoneCallButton: UIButton!
    @IBOutlet private weak var mapView: MKMapView!

    // MARK: Init
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupInitialBindings()
        setupViewModelBindings()
    }
    
    func setup(viewModel: UserDetailInfoViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    // MARK: Properties
    
    private var viewModel: UserDetailInfoViewModel?
    private let cellViewModels = BehaviorRelay<[UserInfoPreviewCellViewModel]>(value: [])
    private let baseBag = DisposeBag()
    private var refreshBag = DisposeBag()
    private let bag: DisposeBag = DisposeBag()

    // MARK: Helpers
    
    private func commonInit() {
        Bundle.main.loadNibNamed(UserDetailInfoView.nibFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }

    private func setupInitialBindings() {
        VisibilityService
            .shared
            .currentLanguage
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (_) in
                self?.agePlaceholderLabel.text = Constants.LabelText.localizedString(.ageUpper) + ":"
                self?.genderPlaceholderLabel.text = Constants.LabelText.localizedString(.genderUpper) + ":"
                self?.emailPlaceholderLabel.text = Constants.LabelText.localizedString(.emailUpper) + ":"
                self?.phonePlaceholderLabel.text = Constants.LabelText.localizedString(.phoneNumer) + ":"
                self?.addressPlaceholderLabel.text = Constants.LabelText.localizedString(.addressUpper) + ":"
            })
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentMainColor
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (color) in
                self?.agePlaceholderLabel.textColor = color.getUIColor()
                self?.genderPlaceholderLabel.textColor = color.getUIColor()
                self?.emailPlaceholderLabel.textColor = color.getUIColor()
                self?.phonePlaceholderLabel.textColor = color.getUIColor()
                self?.addressPlaceholderLabel.textColor = color.getUIColor()
            })
            .disposed(by: bag)
    }
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()
        
        guard let viewModel = viewModel else {
            return
        }
        
        emailSendButton
            .rx
            .tap
            .bind(to: viewModel.emailSendBtnTap)
            .disposed(by: refreshBag)
        
        phoneCallButton
            .rx
            .tap
            .bind(to: viewModel.phoneBtnTap)
            .disposed(by: refreshBag)
        
        viewModel
            .imageUrl
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (urlOptional) in
                self?.imageView.sd_setImage(with: urlOptional, placeholderImage: UIImage(named: Constants.ImageName.userPlacholder.rawValue), options: [], context: nil)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .age
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: ageValueLabel.rx.text)
            .disposed(by: refreshBag)
        
        viewModel
            .gender
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: genderValueLabel.rx.text)
            .disposed(by: refreshBag)
        
        viewModel
            .email
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: emailValueLabel.rx.text)
            .disposed(by: refreshBag)
        
        viewModel
            .phoneNumber
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: phoneValueLabel.rx.text)
            .disposed(by: refreshBag)
        
        viewModel
            .locationDescription
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: addressValueLabel.rx.text)
            .disposed(by: refreshBag)
        
        viewModel
            .location
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (location) in
                self?.configureMapView(for: location)
            })
            .disposed(by: refreshBag)
    }
    
    private func configureMapView(for location: CLLocationCoordinate2D) {
        mapView.removeAnnotations(mapView.annotations)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        
        mapView.addAnnotation(annotation)
        mapView.centerCoordinate = location
    }
}
