//
//  UserListView.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import UIKit
import RxSwift
import RxCocoa

class UserListView: UIView {
    static let nibFileName = "UserListView"
    
    // MARK: Outlets
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var tableView: UITableView!

    // MARK: Init
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureTableView()
        setupRefreshControl()
        setupIntialBindings()
        setupViewModelBindings()
    }
    
    func setup(viewModel: UserListViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    // MARK: Properties
    
    private weak var viewModel: UserListViewModel?
    private let cellViewModels = BehaviorRelay<[UserInfoPreviewCellViewModel]>(value: [])
    private let baseBag = DisposeBag()
    private var refreshBag = DisposeBag()
    private let refreshControl = UIRefreshControl()
    
    // MARK: Helpers
    
    private func commonInit() {
        Bundle.main.loadNibNamed(UserListView.nibFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.alwaysBounceVertical = true
        tableView.delaysContentTouches = false
        
        tableView.register(UINib(nibName: UserInfoPreviewTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: UserInfoPreviewTableViewCell.reuseId)
    }
    
    private func setupRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(handleRefreshControlDrag), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc private func handleRefreshControlDrag() {
        viewModel?.refresh.accept(())
    }
    
    private func setupIntialBindings() {
        cellViewModels
            .subscribe(onNext: { [weak self] (_) in
                self?.tableView.reloadData()
            })
            .disposed(by: baseBag)
    }
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()
        
        guard let viewModel = viewModel else {
            return
        }
        
        viewModel
            .isRefreshing
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: refreshControl.rx.isRefreshing)
            .disposed(by: refreshBag)
        
        viewModel
            .cellViewModels
            .compactMap({ $0 })
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: cellViewModels)
            .disposed(by: refreshBag)
    }
}

// MARK: - UITableViewDataSource

extension UserListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellToReturn = userInfoCell(for: indexPath)
        
        return cellToReturn
    }
    
    private func userInfoCell(for indexPath: IndexPath) -> UITableViewCell {
        guard let userInfoCell = tableView.dequeueReusableCell(withIdentifier: UserInfoPreviewTableViewCell.reuseId, for: indexPath) as? UserInfoPreviewTableViewCell else {
            return UITableViewCell()
        }
        
        let cellViewModel = cellViewModels.value[indexPath.row]
        userInfoCell.setup(viewModel: cellViewModel)
        
        return userInfoCell
    }
}

// MARK: - UITableViewDelegate

extension UserListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UserInfoPreviewTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        viewModel?.tapAtIndex.accept(index)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

