//
//  UserListViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift

// MARK: - UserListViewModel

class UserListViewModel {
    // MARK: Static Methods
    
    static func getChildInstance(for type: UserDataManager.InstanceType) -> UserListViewModel {
        switch type {
            case .local:
                return UserListLocalViewModel()
                
            case .remote:
                return UserListRemoteViewModel()
        }
    }
    
    // MARK: Nested Types
    
    typealias TappedUserInfo = (user: User, userId: String)
    
    // MARK: Input
    
    public let tapAtIndex = PublishRelay<Int>()
    public let searchFilter = BehaviorRelay<String?>(value: nil)
    public let refresh = PublishRelay<Void>()
    
    // MARK: Output
    
    public let cellTapUserInfo = PublishRelay<TappedUserInfo>()
    public let cellViewModels = BehaviorRelay<[UserInfoPreviewCellViewModel]?>(value: nil)
    public let refreshError = PublishRelay<Error>()
    public let writeError = PublishRelay<Error>()
    public let isRefreshing = BehaviorRelay<Bool>(value: true)
    public let isWritting = BehaviorRelay<Bool>(value: false)

    // MARK: Init
    
    fileprivate init() {
        setupInitialBindings()
    }
    
    // MARK: Properties
    
    fileprivate let bag = DisposeBag()
    fileprivate let cellViewModelsNotFiltered = BehaviorRelay<[UserInfoPreviewCellViewModel]?>(value: nil)
    fileprivate var refreshBag = DisposeBag()
    fileprivate let refreshSuccess = PublishRelay<Void>()
    fileprivate let writeSuccess = PublishRelay<Void>()
    fileprivate let cellViewModelHeartIconTap = PublishRelay<UserInfoPreviewCellViewModel>()
    fileprivate lazy var helperQueue = DispatchQueue.global(qos: .default)
    fileprivate lazy var helperScheduler = SerialDispatchQueueScheduler.init(queue: helperQueue, internalSerialQueueName: "UserListViewModel")
    
    // MARK: Helpers
    
    fileprivate func setupInitialBindings() {
        refresh
            .subscribe(onNext: { [weak self] in
                self?.refreshData()
            })
            .disposed(by: bag)
        
        refreshError
            .map { (_) in
                return false
            }
            .bind(to: isRefreshing)
            .disposed(by: bag)
        
        refreshSuccess
            .map { (_) in
                return false
            }
            .bind(to: isRefreshing)
            .disposed(by: bag)
        
        writeError
            .map { (_) in
                return false
            }
            .bind(to: isWritting)
            .disposed(by: bag)
        
        writeSuccess
            .map { (_) in
                return false
            }
            .bind(to: isWritting)
            .disposed(by: bag)
    
        writeSuccess
            .bind(to: refresh)
            .disposed(by: bag)
        
        cellViewModels
            .subscribe(onNext: { [weak self] (_) in
                self?.setupRefreshableBindings()
            })
            .disposed(by: bag)
        
        cellViewModelHeartIconTap
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (cellViewModel) in
                self?.handleHeartIconTap(for: cellViewModel)
            })
            .disposed(by: bag)
        
        // Setting bindings for isSaved property
        Observable
            .combineLatest(UserDataManager.shared.storedUserIds, cellViewModels)
            .observe(on: helperScheduler)
            .subscribe(onNext: { (ids, cellViewModels) in
                cellViewModels?.forEach { (cellViewModelIter) in
                    cellViewModelIter.isSavedUser.accept(ids.contains(cellViewModelIter.userId))
                }
            })
            .disposed(by: bag)
        
        // Setting bindings for search filters
        Observable
            .combineLatest(searchFilter, cellViewModelsNotFiltered)
            .observe(on: helperScheduler)
            .map { [weak self] (strOptional, allCellViewModels) -> [UserInfoPreviewCellViewModel]? in
                guard let allCellViewModels = allCellViewModels else {
                    return nil
                }
                
                return self?.getFilteredItems(searchFilter: strOptional, allCellViewModels: allCellViewModels) ?? []
            }
            .bind(to: cellViewModels)
            .disposed(by: bag)
        
        // Setting tap info bindings
        tapAtIndex
            .compactMap { [weak self] (index) in
                guard let cellViewModels = self?.cellViewModels.value else {
                    return nil
                }
                
                let cellViewModel = cellViewModels[index]
                let user = cellViewModel.user
                let info: TappedUserInfo = (user, cellViewModel.userId)

                return info
            }
            .bind(to: cellTapUserInfo)
            .disposed(by: bag)
    }
    
    fileprivate func refreshData() {}
    
    fileprivate func configureData(users: [User]) {
        let cellViewModels = users.compactMap({ UserInfoPreviewCellViewModel(user: $0) })
        
        self.cellViewModelsNotFiltered.accept(cellViewModels)
    }
    
    fileprivate func setupRefreshableBindings() {
        refreshBag = DisposeBag()
        
        guard let cellViewModels = cellViewModels.value else {
            return
        }
        
        let heartIconTapObservables: [Observable<UserInfoPreviewCellViewModel>] = cellViewModels
            .map { $0.heartTap.asObservable() }
        
        Observable
            .merge(heartIconTapObservables)
            .observe(on: helperScheduler)
            .bind(to: cellViewModelHeartIconTap)
            .disposed(by: refreshBag)
    }
    
    fileprivate func handleHeartIconTap(for viewModel: UserInfoPreviewCellViewModel) {
        let mustRemoveItem = viewModel.isSavedUser.value
        
        vibrate(isSaved: !mustRemoveItem)
        isWritting.accept(true)
        
        if (mustRemoveItem) {
            UserDataManager
                .shared
                .removeUsers(with: [viewModel.userId])
                .take(1)
                .subscribe(
                    onNext: { [weak self] () in
                        self?.writeSuccess.accept(())
                    },
                    onError: { [weak self] (error) in
                        self?.writeError.accept(error)
                    }
                )
                .disposed(by: bag)
        }
        else {
            UserDataManager
                .shared
                .storeUser(user: viewModel.user)
                .take(1)
                .subscribe(
                    onNext: { [weak self] () in
                        self?.writeSuccess.accept(())
                    },
                    onError: { [weak self] (error) in
                        self?.writeError.accept(error)
                    }
                )
                .disposed(by: bag)
        }
    }
    
    fileprivate func getFilteredItems(searchFilter: String?, allCellViewModels: [UserInfoPreviewCellViewModel]) -> [UserInfoPreviewCellViewModel] {
        guard let searchFilter = searchFilter,
              !searchFilter.trimmingCharacters(in: .whitespaces).isEmpty,
              !allCellViewModels.isEmpty else {
            return allCellViewModels
        }
        
        let filteredItems = allCellViewModels
            .filter { (viewModelIter) in
                return (viewModelIter.fullName.value?.contains(searchFilter) == true) || (viewModelIter.email.value?.contains(searchFilter) == true) || (viewModelIter.phoneNumber.value?.contains(searchFilter) == true) || (viewModelIter.locationDescription.value?.contains(searchFilter) == true)
            }
        
        return filteredItems
    }
    
    private func vibrate(isSaved: Bool) {
        let style: UIImpactFeedbackGenerator.FeedbackStyle = isSaved ? .heavy : .light
        
        let generator = UIImpactFeedbackGenerator(style: style)
        generator.impactOccurred()
    }
}

// MARK: - UserListRemoteViewModel

class UserListRemoteViewModel: UserListViewModel {
    // MARK: Init
    
    override init() {
        super.init()
    }
    
    // MARK: Helpers
    
    override final fileprivate func setupInitialBindings() {
        super.setupInitialBindings()
        
        cellViewModels
            .compactMap({ $0 })
            .map({ (_) in return () })
            .bind(to: refreshSuccess)
            .disposed(by: bag)
    }

    override final fileprivate func refreshData() {
        super.refreshData()
        
        isRefreshing.accept(true)
        
        UserDataManager
            .shared
            .getUsers()
            .take(1)
            .observe(on: helperScheduler)
            .subscribe(
                onNext: { [weak self] (users) in
                    self?.configureData(users: users)
                },
                onError: { [weak self] (error) in
                    self?.refreshError.accept(error)
                }
            )
            .disposed(by: bag)
    }
}

// MARK: - UserListLocalViewModel

class UserListLocalViewModel: UserListViewModel {
    // MARK: Init
    
    override init() {
        super.init()
    }
    
    // MARK: Helpers
    
    override final fileprivate func setupInitialBindings() {
        super.setupInitialBindings()
        
        Realm
            .objectsNotifier(type: .user, scheduler: helperScheduler)
            .compactMap { (objects) in
                return objects?
                    .filter({ !$0.isInvalidated })
                    .compactMap{ $0 as? User }
                    .sorted(by: {
                        return $0.id < $1.id
                    })
            }
            .subscribe(onNext: { [weak self] (users) in
                self?.configureData(users: users)
            })
            .disposed(by: bag)
    }
    
    override final fileprivate func refreshData() {
        super.refreshData()
        
        isRefreshing.accept(true)
        
        UserDataManager
            .shared
            .updateStoredInstances()
            .take(1)
            .observe(on: helperScheduler)
            .subscribe(
                onNext: { [weak self] (_) in
                    self?.refreshSuccess.accept(())
                },
                onError: { [weak self] (error) in
                    self?.refreshError.accept(error)
                }
            )
            .disposed(by: bag)
    }
}
