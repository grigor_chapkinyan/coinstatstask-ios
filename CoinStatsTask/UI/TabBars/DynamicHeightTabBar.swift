//
//  DynamicHeightTabBar.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import UIKit

class DynamicHeightTabBar: UITabBar {
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        let safeAreaBottomInset = UIApplication.safeAreaInsets().bottom
        
        sizeThatFits.height = (UIScreen.main.bounds.width * Constants.tabBarAspectRatioMultiply) + (safeAreaBottomInset * 1)
        
        return sizeThatFits
    }
}
