//
//  UserInfoPreviewCellViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import Foundation
import RxSwift
import RxCocoa

struct UserInfoPreviewCellViewModel {
    // MARK: Input
    
    public let heartTap = PublishRelay<UserInfoPreviewCellViewModel>()
    public let isSavedUser = BehaviorRelay<Bool>(value: false)
    
    // MARK: Output
    
    public let fullName = BehaviorRelay<String?>(value: nil)
    public let genderIconImageName = BehaviorRelay<Constants.ImageName?>(value: nil)
    public let email = BehaviorRelay<String?>(value: nil)
    public let phoneNumber = BehaviorRelay<String?>(value: nil)
    public let locationDescription = BehaviorRelay<String?>(value: nil)
    public let heartIconImageName = BehaviorRelay<Constants.ImageName?>(value: nil)
    public let imageUrl = BehaviorRelay<URL?>(value: nil)

    // MARK: Init
    
    init?(user: User) {
        guard !user.isInvalidated else { return nil }
        
        self.userId = user._id
        self.user = user
        
        setupInitialBindings()
        configureData(user: user)
    }
    
    // MARK: Properties
    
    let userId: String
    let user: User
    private let bag = DisposeBag()
    
    // MARK: Helpers
    
    private func setupInitialBindings() {
        isSavedUser
            .map { (isSaved) -> Constants.ImageName in
                return isSaved ? .userHeartFilledIcon : .userHeartNormalIcon
            }
            .bind(to: heartIconImageName)
            .disposed(by: bag)
    }
    
    private func configureData(user: User) {
        let imageUrl = URL(string: user.largeImage)
        let fullName = user.firstName + " " + user.lastName
        let email = user.email
        let phoneNumber = user.phone
        let locationDescription = user.country + ", " + user.state + ", " + user.city
        let genderIconName: Constants.ImageName = (user.gender == .male) ? .maleIcon : .femaleIcon
        
        self.fullName.accept(fullName)
        self.email.accept(email)
        self.phoneNumber.accept(phoneNumber)
        self.locationDescription.accept(locationDescription)
        self.genderIconImageName.accept(genderIconName)
        self.imageUrl.accept(imageUrl)
    }
}

// MARK: - Equatable

extension UserInfoPreviewCellViewModel: Equatable {
    static func == (lhs: UserInfoPreviewCellViewModel, rhs: UserInfoPreviewCellViewModel) -> Bool {
        return lhs.user === rhs.user
    }
}
