//
//  UserInfoPreviewTableViewCell.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class UserInfoPreviewTableViewCell: UITableViewCell {
    static let reuseId = "UserInfoPreviewTableViewCell"
    static let nibName = "UserInfoPreviewTableViewCell"
    static var height: CGFloat  {
        let safeAreainsets = UIApplication.safeAreaInsets()
        
        return (UIScreen.main.bounds.width - safeAreainsets.left - safeAreainsets.right) / 4
    }
    
    // MARK: Outlets
    
    @IBOutlet private weak var userImageView: CircleImageView!
    @IBOutlet private weak var genderIconImageView: UIImageView!
    @IBOutlet private weak var heartIconImageView: UIImageView!
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var phoneNumberLabel: UILabel!
    @IBOutlet private weak var locationDescriptionLabel: UILabel!
    
    // MARK: View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        setupInitialBindings()
        setupHeartImageViewTapGesture()
    }
    
    func setup(viewModel: UserInfoPreviewCellViewModel?) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        setupViewModelBindings()
    }
    
    // MARK: Properties
    
    private var refreshBag = DisposeBag()
    private let baseBag = DisposeBag()
    private var viewModel: UserInfoPreviewCellViewModel?
    
    // MARK: Helpers
    
    private func setupInitialBindings() {
        
    }
    
    private func setupHeartImageViewTapGesture() {
        heartIconImageView.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleHeartTap(_:)))
        tapGesture.cancelsTouchesInView = true
        tapGesture.numberOfTouchesRequired = 1
        
        heartIconImageView.addGestureRecognizer(tapGesture)
    }
    
    private func setupViewModelBindings() {
        guard let viewModel = viewModel else { return }
        
        refreshBag = DisposeBag()

        viewModel
            .imageUrl
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (urlOptional) in
                self?.userImageView.sd_setImage(with: urlOptional, placeholderImage: UIImage(named: Constants.ImageName.userPlacholder.rawValue), options: [], context: nil)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .genderIconImageName
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (iconName) in
                self?.genderIconImageView.image = UIImage(named: iconName.rawValue)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .heartIconImageName
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (iconName) in
                self?.heartIconImageView.image = UIImage(named: iconName.rawValue)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .fullName
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (fullName) in
                self?.fullNameLabel.text = fullName
            })
            .disposed(by: refreshBag)
        
        viewModel
            .email
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (email) in
                self?.emailLabel.text = email
            })
            .disposed(by: refreshBag)
        
        viewModel
            .phoneNumber
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (phoneNumber) in
                self?.phoneNumberLabel.text = phoneNumber
            })
            .disposed(by: refreshBag)
        
        viewModel
            .locationDescription
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (locationDescription) in
                self?.locationDescriptionLabel.text = locationDescription
            })
            .disposed(by: refreshBag)
    }
    
    @objc private func handleHeartTap(_ gesture: UITapGestureRecognizer) {
        if let viewModel = viewModel {
            viewModel.heartTap.accept(viewModel)
        }
    }
}
