//
//  ColorSelectCollectionViewCell.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 20.07.21.
//

import UIKit
import RxSwift
import RxCocoa

class ColorSelectCollectionViewCell: UICollectionViewCell {
    static let reuseId = "ColorSelectCollectionViewCell"
    static let nibName = "ColorSelectCollectionViewCell"

    // MARK: View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupViewModelBindings()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        setupViewModelBindings()
    }
    
    func setup(viewModel: ColorSelectCellViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let viewModel = viewModel {
            configureBorders(isEnabled: !viewModel.isSelected.value, color: viewModel.color)
        }
    }
    
    // MARK: Properties
    
    private var refreshBag = DisposeBag()
    private var viewModel: ColorSelectCellViewModel?

    // MARK: Helpers
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()
        
        guard let viewModel = viewModel else {
            return
        }
        
        viewModel
            .isSelected
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (isSelected) in
                self?.configureBorders(isEnabled: !isSelected, color: viewModel.color)
            })
            .disposed(by: refreshBag)
    }
    
    private func configureBorders(isEnabled: Bool, color: Constants.ColorName) {
        contentView.layer.borderColor = UIColor(named: color.rawValue)?.cgColor
        contentView.layer.cornerRadius = contentView.bounds.height / 2

        if (isEnabled) {
            contentView.layer.borderWidth = contentView.bounds.height / 10
            contentView.backgroundColor = .systemBackground
        }
        else {
            contentView.layer.borderWidth = 0
            contentView.backgroundColor = UIColor(named: color.rawValue)
        }
    }
}
