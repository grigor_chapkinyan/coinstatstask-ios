//
//  ColorSelectCellViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 20.07.21.
//

import Foundation
import RxSwift
import RxCocoa

struct ColorSelectCellViewModel {
    // MARK: Output
    
    public let isSelected = BehaviorRelay<Bool>(value: false)
    
    // MARK: Properties
    
    let color: Constants.ColorName
}
