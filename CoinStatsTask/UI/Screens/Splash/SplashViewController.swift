//
//  SplashViewController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.02.21.
//

import UIKit

class SplashViewController: BaseViewController {
    static let storyboardId = "SplashViewController"
    
    // MARK: Outlet
    
    @IBOutlet private weak var logoImageView: UIImageView!
    
    // MARK: View Life Cycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animateLogoImageView { [weak self] in
            self?.goToHomeScreen()
        }
    }
    
    // MARK: Helpers
    
    private func animateLogoImageView(completion: (() -> ())? = nil) {
        let widthRelation: CGFloat = (view.bounds.width / logoImageView.bounds.width)
        let heightRelation: CGFloat = (view.bounds.height / logoImageView.bounds.height)
        let sizeToScale: CGFloat = min(widthRelation, heightRelation)
            
        UIView.animate(
            withDuration: Constants.splashAllAnimationDuration / 2,
            delay: .zero,
            usingSpringWithDamping: 0.5,
            initialSpringVelocity: 0,
            options: []
        )
        {  [weak self] in
            self?.logoImageView.transform = CGAffineTransform(scaleX: sizeToScale, y: sizeToScale)
        }
        completion: { _ in
            UIView.animate(
                withDuration: Constants.splashAllAnimationDuration / 2,
                delay: .zero,
                usingSpringWithDamping: 0.5,
                initialSpringVelocity: 0,
                options: []
            )
            { [weak self] in
                self?.logoImageView.transform = .identity
            }
            completion: { _ in
                completion?()
            }
        }
    }
    
    private func goToHomeScreen() {
        let homeVC = HomeTabBarController()
        
        navigationController?.pushViewController(homeVC, animated: true)
    }
}
