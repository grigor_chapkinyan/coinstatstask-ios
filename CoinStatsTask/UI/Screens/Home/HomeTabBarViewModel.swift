//
//  HomeTabBarViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import Foundation
import RxSwift
import RxCocoa

struct HomeTabBarViewModel {
    // MARK: Output
    
    public let userListRemoteViewModel = BehaviorRelay<UserListSearchableViewModel>(value: UserListSearchableViewModel(type: .remote))
    public let userListLocalViewModel = BehaviorRelay<UserListSearchableViewModel>(value: UserListSearchableViewModel(type: .local))
    public let settingsViewModel = BehaviorRelay<SettingsViewModel>(value: SettingsViewModel())
}
