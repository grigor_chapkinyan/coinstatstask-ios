//
//  UserDetailViewController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import UIKit
import RxSwift
import RxCocoa

class UserDetailViewController: BaseViewController {
    // MARK: Outlets
    
    @IBOutlet private weak var userDetailInfoView: UserDetailInfoView!
    @IBOutlet private weak var scrollView: UIScrollView!

    // MARK: View Life Cycle
    
    func setup(viewModel: UserDetailViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureScrollView()
        setupInitialBindings()
        setupViewModelBindings()
    }
    
    // MARK: Properties
    
    private var viewModel: UserDetailViewModel?
    private var refreshBag: DisposeBag = DisposeBag()
    private let bag: DisposeBag = DisposeBag()

    // MARK: Helpers
    
    private func configureScrollView() {
        scrollView.delaysContentTouches = false
    }
    
    private func setupInitialBindings() {
        VisibilityService
            .shared
            .currentLanguage
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (_) in
                self?.navigationItem.backButtonTitle = Constants.LabelText.localizedString(.backUpper)
            })
            .disposed(by: bag)
    }
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()
        
        guard isViewLoaded,
              let viewModel = viewModel else {
            return
        }
        
        viewModel
            .isLoading
            .observe(on: MainScheduler.instance)
            .bind(to: rx.isLoading)
            .disposed(by: refreshBag)
        
        viewModel
            .userDetailInfoViewModel
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (userDetailInfoViewModel) in
                self?.userDetailInfoView.setup(viewModel: userDetailInfoViewModel)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .fullName
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: navigationItem.rx.title)
            .disposed(by: refreshBag)
        
        viewModel
            .isSavedUser
            .compactMap({ $0 })
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (isSavedUser) in
                self?.configureNavigationItemHeartButton(isFilled: isSavedUser)
            })
            .disposed(by: refreshBag)
    }
    
    private func configureNavigationItemHeartButton(isFilled: Bool) {
        let barButttonImage = isFilled ? UIImage(named: Constants.ImageName.userHeartFilledIcon.rawValue) : UIImage(named: Constants.ImageName.userHeartNormalIcon.rawValue)
        
        let barButtonToSet = UIBarButtonItem(image: barButttonImage, landscapeImagePhone: nil, style: .done, target: self, action: #selector(heartIconTap))
        
        navigationItem.setRightBarButton(barButtonToSet, animated: true)
    }
    
    @objc private func heartIconTap() {
        viewModel?.heartIconTap.accept(())
    }
}
