//
//  UserDetailViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.07.21.
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift
import CoreHaptics

// MARK: - UserListViewModel

class UserDetailViewModel {
    // MARK: Nested Types
    
    enum Error: Swift.Error {
        case unknown(error: Swift.Error)
        case invalidEmail
        case invalidPhoneNumber
        case invalidatedObject
    }
    
    // MARK: Input
    
    public let heartIconTap = PublishRelay<Void>()
    
    // MARK: Output

    public let isSavedUser = BehaviorRelay<Bool?>(value: nil)
    public let error = PublishRelay<Error>()
    public let isLoading = BehaviorRelay<Bool>(value: false)
    public let fullName = BehaviorRelay<String?>(value: nil)
    public lazy var userDetailInfoViewModel = BehaviorRelay<UserDetailInfoViewModel?>(value: nil)

    // MARK: Init

    init?(userInfo: UserListViewModel.TappedUserInfo) {
        guard  !userInfo.user.isInvalidated else {
            return nil
        }
        
        self.userId = userInfo.userId
        self.userInitial = userInfo.user
        
        setupInitialBindings()
        configureInitialState()
    }

    // MARK: Properties

    private let userId: String
    private var userInitial: User
    private let userThreadSafe = BehaviorRelay<User?>(value: nil)
    private let bag = DisposeBag()
    private var refreshBag = DisposeBag()
    private lazy var helperQueue = DispatchQueue.global(qos: .default)
    private lazy var helperScheduler = SerialDispatchQueueScheduler.init(queue: helperQueue, internalSerialQueueName: "UserDetailViewModel")
    
    // MARK: Helpers

    private func configureInitialState() {
        if UserDataManager.shared.storedUserIds.value.contains(userId) {
            Realm
                .threadSafeObject(type: .user, id: userId, scheduler: helperScheduler)
                .take(1)
                .map { (object) in
                    return object as? User
                }
                .bind(to: userThreadSafe)
                .disposed(by: bag)
        }
        else {
            configureData(for: userInitial)
        }
    }
    
    private func setupInitialBindings() {
        UserDataManager
            .shared
            .storedUserIds
            .map ({ [unowned self] in $0.contains(self.userId) })
            .bind(to: isSavedUser)
            .disposed(by: bag)
        
        userThreadSafe
            .observe(on: helperScheduler)
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] (user) in
                self?.configureData(for: user)
            })
            .disposed(by: bag)
        
        Realm
            .objectsNotifier(type: .user, scheduler: helperScheduler)
            .compactMap { (objects) -> User? in
                return objects?
                    .filter({ (!$0.isInvalidated) })
                    .compactMap{ $0 as? User }
                    .filter({ [weak self] in ($0._id == self?.userId) })
                    .first
            }
            .bind(to: userThreadSafe)
            .disposed(by: bag)
        
        userDetailInfoViewModel
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] (_) in
                self?.setupRefreshableBindings()
            })
            .disposed(by: bag)

        heartIconTap
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] () in
                self?.handleHeartIconTap()
            })
            .disposed(by: bag)
        
        error
            .map({ (_) in return false })
            .bind(to: isLoading)
            .disposed(by: bag)
    }
    
    private func configureData(for user: User) {
        guard  !user.isInvalidated else {
            return
        }
        
        isLoading.accept(true)
        
        let fullName = user.namePrefix + user.firstName + " " + user.lastName
        let userDetailInfoViewModel = UserDetailInfoViewModel(user: user)

        self.fullName.accept(fullName)
        self.userDetailInfoViewModel.accept(userDetailInfoViewModel)
        
        isLoading.accept(false)
        self.userInitial = user
    }
    
    private func setupRefreshableBindings() {
        refreshBag = DisposeBag()
        
        guard let userDetailInfoViewModel = userDetailInfoViewModel.value else {
            return
        }
        
        userDetailInfoViewModel
            .openMailAppWithStr
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (mail) in
                guard let mail = mail else {
                    self?.error.accept(.invalidEmail)
                    return
                }
                
                UIApplication.openNativeApp(valueToAppend: mail, appType: .mailTo) { [weak self] (success) in
                    if (!success) {
                        self?.error.accept(.invalidEmail)
                    }
                }
            })
            .disposed(by: refreshBag)
        
        userDetailInfoViewModel
            .openPhoneAppWithPhoneNmb
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (phoneNumber) in
                guard let phoneNumber = phoneNumber?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
                    self?.error.accept(.invalidPhoneNumber)
                    return
                }
                
                UIApplication.openNativeApp(valueToAppend: phoneNumber, appType: .phone) { [weak self] (success) in
                    if (!success) {
                        self?.error.accept(.invalidPhoneNumber)
                    }
                }
            })
            .disposed(by: refreshBag)
    }
    
    private func handleHeartIconTap() {
        guard let mustRemoveItem = isSavedUser.value else {
            return
        }
        
        vibrate(isSaved: !mustRemoveItem)
        isLoading.accept(true)
        
        if (mustRemoveItem) {
            Realm
                .threadSafeObject(type: .user, id: userId, scheduler: helperScheduler)
                .take(1)
                .subscribe(onNext: { [weak self] (object) in
                    guard let userInitial = object as? User else {
                        self?.error.accept(.invalidatedObject)
                        return
                    }
                    
                    self?.userInitial = User(value: userInitial)
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    UserDataManager
                        .shared
                        .removeUsers(with: [strongSelf.userId])
                        .take(1)
                        .subscribe(
                            onNext: { [weak self] () in
                                self?.isLoading.accept(false)
                            },
                            onError: { [weak self] (error) in
                                self?.error.accept(.unknown(error: error))
                            }
                        )
                        .disposed(by: strongSelf.bag)
                })
                .disposed(by: bag)
        }
        else {
            let copiedObject = User(value: userInitial)
        
            UserDataManager
                .shared
                .storeUser(user: copiedObject)
                .take(1)
                .subscribe(
                    onNext: { [weak self] () in
                        self?.isLoading.accept(false)
                    },
                    onError: { [weak self] (error) in
                        self?.error.accept(.unknown(error: error))
                    }
                )
                .disposed(by: bag)
        }
    }
    
    private func vibrate(isSaved: Bool) {
        let style: UIImpactFeedbackGenerator.FeedbackStyle = isSaved ? .heavy : .light
        
        let generator = UIImpactFeedbackGenerator(style: style)
        generator.impactOccurred()
    }
}
