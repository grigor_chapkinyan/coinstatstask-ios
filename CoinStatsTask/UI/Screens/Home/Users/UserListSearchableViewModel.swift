//
//  UserListSearchableViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import Foundation
import RxSwift
import RxCocoa

class UserListSearchableViewModel {
    // MARK: Input
    
    public let searchFilter = BehaviorRelay<String?>(value: nil)
    public let refresh = PublishRelay<Void>()
    
    // MARK: Output
    
    public let isLoading = BehaviorRelay<Bool>(value: false)
    public lazy var userListViewModel = BehaviorRelay<UserListViewModel>(value: UserListViewModel.getChildInstance(for: type))
    
    // MARK: Init
    
    init(type: UserDataManager.InstanceType) {
        self.type = type
        
        setupInitialBindings()
    }
    
    // MARK: Properties
    
    let type: UserDataManager.InstanceType
    private let bag = DisposeBag()
    
    // MARK: Helpers
    
    private func setupInitialBindings() {
        refresh
            .bind(to: userListViewModel.value.refresh)
            .disposed(by: bag)
        
        userListViewModel
            .value
            .isWritting
            .bind(to: isLoading)
            .disposed(by: bag)
        
        searchFilter
            .bind(to: userListViewModel.value.searchFilter)
            .disposed(by: bag)
    }
}
