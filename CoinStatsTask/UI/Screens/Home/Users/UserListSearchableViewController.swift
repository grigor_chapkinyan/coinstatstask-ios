//
//  UserListSearchableViewController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import UIKit
import RxSwift
import RxCocoa

class UserListSearchableViewController: BaseViewController {
    // MARK: Outlets
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var userListView: UserListView!

    // MARK: View Life Cycle
    
    func setup(viewModel: UserListSearchableViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInitialBindings()
        setupViewModelBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel?.refresh.accept(())
    }
    
    // MARK: Methods
    
    func getViewModelType() -> UserDataManager.InstanceType? {
        return viewModel?.type
    }
    
    // MARK: Properties
    
    private var viewModel: UserListSearchableViewModel?
    private var refreshBag: DisposeBag = DisposeBag()
    private let bag: DisposeBag = DisposeBag()

    // MARK: Helpers
    
    private func setupInitialBindings() {
        VisibilityService
            .shared
            .currentLanguage
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (_) in
                self?.searchBar.placeholder = Constants.LabelText.localizedString(.searchBy)
            })
            .disposed(by: bag)
    }
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()
        
        guard isViewLoaded,
              let viewModel = viewModel else {
            return
        }
        
        viewModel
            .isLoading
            .observe(on: MainScheduler.instance)
            .bind(to: rx.isLoading)
            .disposed(by: refreshBag)
        
        viewModel
            .userListViewModel
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (userListViewModel) in
                self?.userListView.setup(viewModel: userListViewModel)
            })
            .disposed(by: refreshBag)
        
        searchBar
            .rx
            .text
            .bind(to: viewModel.searchFilter)
            .disposed(by: refreshBag)
        
        viewModel
            .userListViewModel
            .value
            .cellTapUserInfo
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (userInfo) in
                self?.goToUserDetailScreen(userInfo: userInfo)
            })
            .disposed(by: refreshBag)
    }
    
    private func goToUserDetailScreen(userInfo: UserListViewModel.TappedUserInfo) {
        guard let userDetailViewModel = UserDetailViewModel(userInfo: userInfo) else {
            return
        }
        
        let userDetailVC = UserDetailViewController.loadFromNib()
        userDetailVC.setup(viewModel: userDetailViewModel)
        
        navigationController?.pushViewController(userDetailVC, animated: true)
    }
}
