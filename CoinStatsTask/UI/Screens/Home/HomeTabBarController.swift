//
//  HomeTabBarController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 16.07.21.
//

import UIKit
import RxCocoa
import RxSwift

class HomeTabBarController: UITabBarController {
    // MARK: Static Properties
    
    static weak var shared: HomeTabBarController?
    
    // MARK: Nested Types
    
    private typealias TabBarItemConfiguration = (index: Int, title: String, deactiveImage: UIImage, activeImage: UIImage)
    
    private enum ChildVcType: Int, CaseIterable {
        case homeRemote = 0
        case homeLocal
        case settings
        
        static func navBarItemTitleForItem(at index: Int) -> String? {
            guard let instance = ChildVcType(rawValue: index) else {
                return nil
            }
            
            var titleToReturn: String
            
            switch instance {
                case .homeRemote:
                    titleToReturn = Constants.LabelText.localizedString(.users)
                    
                case .homeLocal:
                    titleToReturn = Constants.LabelText.localizedString(.saved)
                    
                case .settings:
                    titleToReturn = Constants.LabelText.localizedString(.settings)
            }
            
            return titleToReturn
        }
        
        static func tabBarItemTitleForItem(at index: Int) -> String? {
            guard let instance = ChildVcType(rawValue: index) else {
                return nil
            }
            
            var titleToReturn: String
            
            switch instance {
                case .homeRemote:
                    titleToReturn = Constants.LabelText.localizedString(.users)

                case .homeLocal:
                    titleToReturn = Constants.LabelText.localizedString(.saved)

                case .settings:
                    titleToReturn = Constants.LabelText.localizedString(.settings)
            }
            
            return titleToReturn
        }
        
        static func tabBarItemImageForItem(at index: Int) -> UIImage? {
            guard let instance = ChildVcType(rawValue: index) else {
                return nil
            }
            
            var imageToReturn: UIImage?
            
            switch instance {
                case .homeRemote:
                    imageToReturn = UIImage(named: Constants.ImageName.usersIcon.rawValue)
                
                case .homeLocal:
                    imageToReturn = UIImage(named: Constants.ImageName.heartIcon.rawValue)

                case .settings:
                    imageToReturn = UIImage(named: Constants.ImageName.settingsIcon.rawValue)
            }
            
            return imageToReturn
        }
    }
    
    // MARK: View Life Cycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        object_setClass(self.tabBar, DynamicHeightTabBar.self)
        HomeTabBarController.shared = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        HomeTabBarController.shared = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInitialState()
        configureTabBarVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavBarVisibility(navControllerWasConfigurred: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureNavController()
    }
    
    open override var selectedViewController: UIViewController? {
        willSet {
            configureNavItemTitle(for: newValue)
        }
    }
    
    open override var viewControllers: [UIViewController]? {
        didSet {
            configureNavItemTitle(for: viewControllers?.first)
        }
    }
    
    // MARK: Methods
    
    func reset() {
        lastSelectedIndex = selectedIndex

        DispatchQueue.main.async { [weak self] in
            self?.setupInitialState()

            let viewControllersCount = self?.viewControllers?.count ?? 0

            if let lastSelectedIndex = self?.lastSelectedIndex,
               lastSelectedIndex < viewControllersCount  {
                self?.selectedIndex = lastSelectedIndex
            }
        }
    }
    
    // MARK: Properties
    
    private let viewModel: HomeTabBarViewModel = HomeTabBarViewModel()
    private let bag = DisposeBag()
    private var lastSelectedIndex: Int?
    
    // MARK: Helpers
    
    private func setupInitialState() {
        setupViewControllers()
        configureTabBarItemsProperties()
        setupInitialBindings()
    }
    
    private func setupViewControllers() {
        viewControllers = []
        
        let viewControllersToSet = getChildViewControllersToSet()
        
        viewControllers = viewControllersToSet
    }
    
    private func configureTabBarItemsProperties() {
        let configsToSet = getTabBarItemConfigsToSet()
        
        tabBar.items?.enumerated().forEach({ (index, item) in
            guard let configToSet = configsToSet.filter({ $0.index == index }).first else {
                return
            }
            
            item.title = configToSet.title
            item.image = configToSet.deactiveImage
            item.selectedImage = configToSet.activeImage
        })
    }
    
    private func configureTabBarVisibility() {
        tabBar.isTranslucent = false
    }
    
    private func setupInitialBindings() {
        
    }
    
    private func configureNavController() {
        navigationController?.viewControllers = [self]
        configureNavBarVisibility(navControllerWasConfigurred: true)
    }
    
    private func configureNavBarVisibility(navControllerWasConfigurred: Bool) {
        navigationController?.isNavigationBarHidden = false
        navigationItem.hidesBackButton = navControllerWasConfigurred ? false : true
    }
    
    private func getChildViewControllersToSet() -> [UIViewController] {
        let viewControllersToReturn: [UIViewController] = ChildVcType
            .allCases
            .enumerated()
            .sorted(by: { $0.offset < $1.offset })
            .map ({ (_, vcType) in
                return getChildViewController(vcType)
            })
        
        return viewControllersToReturn
    }
    
    private func getChildViewController(_ type: ChildVcType) -> UIViewController {
        let storedVc = getChildViewControllerFromExistChilds(type)
        
        if let storedVc = storedVc {
            return storedVc
        }
        else {
            let vcToReturn = setupAndGetChildVc(type)
            
            return vcToReturn
        }
    }
    
    private func getChildViewControllerFromExistChilds(_ type: ChildVcType) -> UIViewController? {
        var vcToReturn: UIViewController?
        
        switch type {
            case .homeRemote:
                vcToReturn = viewControllers?
                    .compactMap({ $0 as? UserListSearchableViewController })
                    .filter({ ($0.getViewModelType() == .remote) })
                    .first
                
            case .homeLocal:
                vcToReturn = viewControllers?
                    .compactMap({ $0 as? UserListSearchableViewController })
                    .filter({ ($0.getViewModelType() == .local) })
                    .first
                
            case .settings:
                vcToReturn = viewControllers?
                    .compactMap({ $0 as? SettingsViewController })
                    .first
        }
        
        return vcToReturn
    }
    
    private func setupAndGetChildVc(_ type: ChildVcType) -> UIViewController {
        var vcToReturn: UIViewController
        
        switch type {
            case .homeRemote:
                let userListVc = UserListSearchableViewController.loadFromNib()
                
                let userListViewModel = self.viewModel.userListRemoteViewModel.value
                userListVc.setup(viewModel: userListViewModel)
                
                vcToReturn = userListVc
                
            case .homeLocal:
                let userListVc = UserListSearchableViewController.loadFromNib()
                
                let userListViewModel = self.viewModel.userListLocalViewModel.value
                userListVc.setup(viewModel: userListViewModel)
                
                vcToReturn = userListVc
                
            case .settings:
                let settingsVc = SettingsViewController.loadFromNib()
                
                let settingsViewModel = self.viewModel.settingsViewModel.value
                settingsVc.setup(viewModel: settingsViewModel)
                
                vcToReturn = settingsVc
        }
        
        return vcToReturn
    }
    
    private func getTabBarItemConfigsToSet() -> [TabBarItemConfiguration] {
        let itemsToReturn = ChildVcType
            .allCases
            .enumerated()
            .sorted(by: { $0.offset < $1.offset })
            .compactMap({ return getTabBarItemsConfiguration(for: $0.offset) })
        
        return itemsToReturn
    }
    
    private func getTabBarItemsConfiguration(for index: Int) -> TabBarItemConfiguration? {
        guard let title = ChildVcType.tabBarItemTitleForItem(at: index),
              let iconImage = ChildVcType.tabBarItemImageForItem(at: index),
              let currentMainColor = UIColor(named: VisibilityService.shared.currentMainColor.value.rawValue) else {
            return nil
        }
        
        let deactiveImage = iconImage.withTintColor(.systemGray)
        let activeImage = iconImage.withTintColor(currentMainColor)

        let tabBarItemConfigToReturn = TabBarItemConfiguration(index: index, title: title, deactiveImage: deactiveImage, activeImage: activeImage)

        return tabBarItemConfigToReturn
    }
    
    private func configureNavItemTitle(for selectedVc: UIViewController?) {
        let vcIndex = viewControllers?
            .enumerated()
            .filter({ $0.element === selectedVc })
            .map({ $0.offset })
            .first
        
        guard let vcIndex = vcIndex else {
            return
        }

        navigationItem.title = ChildVcType.navBarItemTitleForItem(at: vcIndex)
    }
}
