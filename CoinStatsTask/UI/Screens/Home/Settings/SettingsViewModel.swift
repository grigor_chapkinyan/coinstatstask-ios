//
//  SettingsViewModel.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import Foundation
import RxSwift
import RxCocoa
import LanguageManager_iOS

class SettingsViewModel {
    // MARK: Input
    
    public let selectedThemeIndexChange = PublishRelay<Int>()
    public let selectedColorIndexChange = PublishRelay<Int>()
    public let selectedLanguageIndexChange = PublishRelay<Int>()

    // MARK: Output
    
    public let isLoading = BehaviorRelay<Bool>(value: true)
    public let languages = BehaviorRelay<[Constants.AppLanguage]>(value: Constants.AppLanguage.allCases)
    public let themes = BehaviorRelay<[UIUserInterfaceStyle]>(value: UIUserInterfaceStyle.allCases)
    public let selectedLanguageIndex = BehaviorRelay<Int?>(value: nil)
    public let selectedThemeIndex = BehaviorRelay<Int?>(value: nil)
    public let selectedColorIndex = BehaviorRelay<Int?>(value: nil)
    public let colorSelectCellViewModels = BehaviorRelay<[ColorSelectCellViewModel]?>(value: nil)

    // MARK: Init
    
    init() {
        setupInitialBindings()
    }
    
    // MARK: Properties
    
    private var lastSelectedLanguage: Languages?
    private let selectedLanguage = BehaviorRelay<Languages?>(value: nil)
    private let selectedTheme = BehaviorRelay<UIUserInterfaceStyle?>(value: nil)
    private let selectedColor = BehaviorRelay<Constants.ColorName?>(value: nil)
    private let colors = BehaviorRelay<[Constants.ColorName]>(value: Constants.ColorName.allCases)
    private let bag = DisposeBag()
    
    // MARK: Helpers
    
    private func setupInitialBindings() {
        Observable
            .combineLatest(selectedLanguageIndex, selectedColorIndex, selectedThemeIndex)
            .map { ($0.0 == nil) || ($0.1 == nil) || ($0.2 == nil) }
            .bind(to: isLoading)
            .disposed(by: bag)
        
        // Configuring selected items
        
        VisibilityService
            .shared
            .currentMainColor
            .bind(to: selectedColor)
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentTheme
            .bind(to: selectedTheme)
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentLanguage
            .bind(to: selectedLanguage)
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentLanguage
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (language) in
                if let lastSelectedLanguage = self?.lastSelectedLanguage {
                    if language != lastSelectedLanguage {
                        HomeTabBarController.shared?.reset()
                    }
                }
                
                self?.lastSelectedLanguage = language
            })
            .disposed(by: bag)
        
        // Configuring selected items indexes

       Observable
        .combineLatest(colors, selectedColor)
        .map { (colors, selectedColor) in
            return colors.enumerated().filter({ $0.element == selectedColor }).map({ $0.offset }).first
        }
        .bind(to: selectedColorIndex)
        .disposed(by: bag)
        
        Observable
            .combineLatest(themes, selectedTheme)
            .map { (themes, selectedTheme) in
                return themes.enumerated().filter({ $0.element == selectedTheme }).map({ $0.offset }).first
            }
            .bind(to: selectedThemeIndex)
            .disposed(by: bag)
        
        Observable
            .combineLatest(languages, selectedLanguage)
            .map { (languages, selectedLanguage) in
                return languages.enumerated().filter({ $0.element.getManagerLanguageInstance() == selectedLanguage }).map({ $0.offset }).first
            }
            .bind(to: selectedLanguageIndex)
            .disposed(by: bag)
        
        // Configuring index change bindings
        
        selectedColorIndexChange
            .compactMap { [weak self] (index) in
                return self?.colors.value[index]
            }
            .bind(to: VisibilityService.shared.mainColorChange)
            .disposed(by: bag)
        
        selectedThemeIndexChange
            .compactMap { [weak self] (index) in
                return self?.themes.value[index]
            }
            .bind(to: VisibilityService.shared.themeChange)
            .disposed(by: bag)
        
        selectedLanguageIndexChange
            .compactMap { [weak self] (index) in
                guard let languageRawValue = self?.languages.value[index].rawValue else {
                    return nil
                }
                 
                return Languages(rawValue: languageRawValue)
            }
            .bind(to: VisibilityService.shared.defaultLanguageChange)
            .disposed(by: bag)
        
        colors
            .map({ [weak self] in
                let index = self?.selectedColorIndex.value ?? 0
                let viewModelsToReturn = $0.map{ ColorSelectCellViewModel(color: $0) }
                
                viewModelsToReturn.enumerated().forEach({
                    $0.1.isSelected.accept(($0.0 == index))
                })
                
                return viewModelsToReturn
            })
            .bind(to: colorSelectCellViewModels)
            .disposed(by: bag)
        
        Observable
            .combineLatest(colorSelectCellViewModels, selectedColorIndex)
            .subscribe(onNext: { (cellViewModels, index) in
                cellViewModels?.enumerated().forEach({
                    $0.1.isSelected.accept(($0.0 == index))
                })
            })
            .disposed(by: bag)
    }
}
