//
//  SettingsViewController.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 17.07.21.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsViewController: BaseViewController {
    // MARK: Outlets
    
    @IBOutlet private weak var languageSegmentControl: UISegmentedControl!
    @IBOutlet private weak var appearanceModeSegmentControl: UISegmentedControl!
    @IBOutlet private weak var colorsCollectionView: UICollectionView!
    @IBOutlet private weak var mainColorPlacholderLabel: UILabel!
    @IBOutlet private weak var languagePlacholderLabel: UILabel!
    @IBOutlet private weak var appearancePlacholderLabel: UILabel!
    @IBOutlet private weak var setDefaultButton: UIButton!

    // MARK: View Life Cycle
    
    func setup(viewModel: SettingsViewModel) {
        self.viewModel = viewModel
        
        setupViewModelBindings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confiugreInitialStates()
        setupInitialBindings()
        setupViewModelBindings()
    }
    
    // MARK: IBActions
    
    @IBAction private func refreshTap() {
        VisibilityService.shared.resetSettings.accept(())
    }

    // MARK: Properties
    
    private var viewModel: SettingsViewModel?
    private let bag: DisposeBag = DisposeBag()
    private var refreshBag: DisposeBag = DisposeBag()
    private let colorSelectCellViewModels = BehaviorRelay<[ColorSelectCellViewModel]>(value: [])

    // MARK: Helpers
    
    private func confiugreInitialStates() {
        // Configuring segment controls
        languageSegmentControl.addTarget(self, action: #selector(languageSegmentControlHandler(_:)), for: .valueChanged)
        appearanceModeSegmentControl.addTarget(self, action: #selector(themeSegmentControlHandler(_:)), for: .valueChanged)
        
        // Configuring collection view
        colorsCollectionView.dataSource = self
        colorsCollectionView.delegate = self
        colorsCollectionView.register(UINib(nibName: ColorSelectCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: ColorSelectCollectionViewCell.reuseId)
        
        // Configuring setDefault button
        setDefaultButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    private func setupInitialBindings() {
        colorSelectCellViewModels
            .subscribe(onNext: { [weak self] (_) in
                self?.colorsCollectionView.reloadData()
            })
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentLanguage
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (_) in
                self?.mainColorPlacholderLabel.text = Constants.LabelText.localizedString(.mainColor)
                self?.languagePlacholderLabel.text = Constants.LabelText.localizedString(.language)
                self?.appearancePlacholderLabel.text = Constants.LabelText.localizedString(.appearance)
                self?.setDefaultButton.setTitle(Constants.LabelText.localizedString(.setDeafult), for: .normal)
            })
            .disposed(by: bag)
        
        VisibilityService
            .shared
            .currentMainColor
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (color) in
                self?.languageSegmentControl.selectedSegmentTintColor = color.getUIColor()
                self?.appearanceModeSegmentControl.selectedSegmentTintColor = color.getUIColor()
            })
            .disposed(by: bag)
    }
    
    private func setupViewModelBindings() {
        refreshBag = DisposeBag()

        guard isViewLoaded,
              let viewModel = viewModel else {
            return
        }
        
        viewModel
            .languages
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (languages) in
                self?.configureLanguageSegmentControl(for: languages)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .themes
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (themes) in
                self?.configureThemeSegmentControl(for: themes)
            })
            .disposed(by: refreshBag)
        
        viewModel
            .colorSelectCellViewModels
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: colorSelectCellViewModels)
            .disposed(by: refreshBag)
        
        viewModel
            .isLoading
            .observe(on: MainScheduler.instance)
            .bind(to: rx.isLoading)
            .disposed(by: refreshBag)
        
        viewModel
            .selectedLanguageIndex
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: languageSegmentControl.rx.selectedSegmentIndex)
            .disposed(by: refreshBag)
        
        viewModel
            .selectedThemeIndex
            .compactMap({ $0 })
            .observe(on: MainScheduler.instance)
            .bind(to: appearanceModeSegmentControl.rx.selectedSegmentIndex)
            .disposed(by: refreshBag)
    }

    private func configureLanguageSegmentControl(for languages: [Constants.AppLanguage]) {
        languageSegmentControl.removeAllSegments()
        
        languages.enumerated().forEach({ languageSegmentControl.insertSegment(withTitle: $0.1.title(), at: $0.0, animated: false) })
    }
    
    private func configureThemeSegmentControl(for themes: [UIUserInterfaceStyle]) {
        appearanceModeSegmentControl.removeAllSegments()
        
        themes.enumerated().forEach({ appearanceModeSegmentControl.insertSegment(withTitle: $0.1.getTitle(), at: $0.0, animated: false) })
    }
    
    @objc private func languageSegmentControlHandler(_ control: UISegmentedControl) {
        let selectedIndex = control.selectedSegmentIndex
        
        viewModel?.selectedLanguageIndexChange.accept(selectedIndex)
    }
    
    @objc private func themeSegmentControlHandler(_ control: UISegmentedControl) {
        let selectedIndex = control.selectedSegmentIndex
        
        viewModel?.selectedThemeIndexChange.accept(selectedIndex)
    }
}

// MARK: - UICollectionViewDataSource

extension SettingsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorSelectCellViewModels.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return colorSelectCell(for: indexPath)
    }
    
    private func colorSelectCell(for indexPath: IndexPath) -> UICollectionViewCell {
        guard let colorSelectCell = colorsCollectionView.dequeueReusableCell(withReuseIdentifier: ColorSelectCollectionViewCell.reuseId, for: indexPath) as? ColorSelectCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let cellViewModel = colorSelectCellViewModels.value[indexPath.row]
        colorSelectCell.setup(viewModel: cellViewModel)
        
        return colorSelectCell
    }
}

// MARK: - UICollectionViewDelegate

extension SettingsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel?.selectedColorIndexChange.accept(indexPath.row)
    }
}

 
