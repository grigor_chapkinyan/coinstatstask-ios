//
//  Realm + Ext.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 11/11/20.
//

import Foundation
import RealmSwift
import Realm
import RxSwift
import RxCocoa

internal protocol CascadingDeletable: RealmSwift.Object {
    static var propertiesToCascadeDelete: [String] { get }
}

extension Realm {
    func cascadingDelete(_ object: RealmSwift.Object) {
        var toBeDeleted: Set<RLMObjectBase> = [object]
        while let element = toBeDeleted.popFirst() as? RealmSwift.Object {
            guard !element.isInvalidated else { continue }
            if let cascadingDeletable = element as? CascadingDeletable {
                cascade(into: cascadingDeletable, toBeDeleted: &toBeDeleted)
            }
            delete(element)
        }
    }
    
    func cascadingDelete(_ objects: [RealmSwift.Object]) {
        for object in objects {
            cascadingDelete(object)
        }
    }
    
    private func cascade(into object: CascadingDeletable, toBeDeleted: inout Set<RLMObjectBase>) {
        let objectType = type(of: object)
        
        guard let schema = objectType.sharedSchema() else { return }
        
        let primaryKey = objectType.primaryKey()
        let primaryKeyValue = primaryKey.flatMap(object.value(forKey:))
        
        let properties = (schema.properties + schema.computedProperties)
            .filter { objectType.propertiesToCascadeDelete.contains($0.name) }
        
        for property in properties {
            switch object.value(forKey: property.name) {
                case let realmObject as RLMObjectBase:
                    toBeDeleted.insert(realmObject)
                case let list as RLMSwiftCollectionBase:
                    for index in 0 ..< list._rlmCollection.count {
                        guard let realmObject = list._rlmCollection.object(at: index) as? RLMObjectBase else { continue }
                        toBeDeleted.insert(realmObject)
                    }
                default: // LinkingObjects
                    if let linkOriginPropertyName = property.linkOriginPropertyName,
                       let linkOriginTypeName = property.objectClassName,
                       let primaryKey = primaryKey,
                       let primaryKeyValue = primaryKeyValue {
                        dynamicObjects(linkOriginTypeName)
                            .filter("%K == %@", "\(linkOriginPropertyName).\(primaryKey)", primaryKeyValue)
                            .forEach { toBeDeleted.insert($0) }
                    }
            }
        }
    }
}

extension Realm {
    enum RealmObjectType {
        case user
    }
    
    static func objectsNotifier(type: RealmObjectType, scheduler: SchedulerType) -> Observable<[Object]?> {
        return objectsChangeNotifier(type: type)
            .flatMap { (value) in
                return value
            }
            .flatMap {
                return objects(type: type,scheduler: scheduler)
            }
            .subscribe(on: scheduler)
    }
    
    static func objects(type: RealmObjectType, scheduler: SchedulerType) -> Observable<[Object]?> {
        return objectsRef(type: type)
            .observe(on: scheduler)
            .map { (refAnyOptional) in
                switch type {
                    case .user:
                        guard   let ref = refAnyOptional as? ThreadSafeReference<Results<User>> else { return nil }
                        
                        let realm = try! Realm()
                        let initialResult = realm.resolve(ref)?.toArray() ?? []
                        
                        return initialResult
                }
            }
            .subscribe(on: scheduler)
            .take(1)
    }
    
    static func threadSafeObject(type: RealmObjectType, id: String, scheduler: SchedulerType) -> Observable<Object?> {
        return objectRef(type: type, id: id)
            .observe(on: scheduler)
            .map { (refAnyOptional) in
                switch type {
                    case .user:
                        guard   let ref = refAnyOptional else { return nil }
                        
                        let realm = try! Realm()
                        let realmObjectOptional = realm.resolve(ref)
                        let user = realmObjectOptional as? User
                        
                        return user
                }
            }
            .subscribe(on: scheduler)
            .take(1)
    }
    
    private static func objectsRef(type: RealmObjectType) -> Observable<Any?> {
        return Observable.create { (observer) -> Disposable in
            autoreleasepool {
                switch type {
                    case .user:
                        let collection = DBService.shared.realm.objects(User.self)
                        let ref = ThreadSafeReference(to: collection)
                        
                        observer.onNext(ref)
                        observer.onCompleted()
                }
                
                return Disposables.create {}
            }
        }
        .subscribe(on: DBService.shared.helperScheduler)
    }
    
    private static func objectRef(type: RealmObjectType, id: String) -> Observable<ThreadSafeReference<RealmObjectWithId>?> {
        return Observable.create { (observer) -> Disposable in
            autoreleasepool {
                switch type {
                    case .user:
                        guard let user = DBService.shared.realm.object(ofType: User.self, forPrimaryKey: id) else {
                            observer.onNext(nil)
                            observer.onCompleted()

                            return Disposables.create{}
                        }
                        
                        let realmObject = user as RealmObjectWithId
                        let ref = ThreadSafeReference(to: realmObject)
                        
                        observer.onNext(ref)
                        observer.onCompleted()
                }
                
                return Disposables.create{}
            }
        }
        .subscribe(on: DBService.shared.helperScheduler)
    }
    
    private static func objectsChangeNotifier(type: RealmObjectType) -> Observable<Observable<Void>> {
        return Observable.create { (observer) -> Disposable in
            autoreleasepool {
                switch type {
                    case .user:
                        let collection = DBService.shared.realm.objects(User.self)
                        let observableToReturn = Observable.changeset(from: collection).map{ (_,_) in return () }
                        
                        observer.onNext(observableToReturn)
                        observer.onCompleted()
                }
                
                return Disposables.create {}
            }
        }
        .subscribe(on: DBService.shared.helperScheduler)
    }
}
