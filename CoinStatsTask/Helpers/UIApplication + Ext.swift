//
//  UIApplication + Ext.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 11/11/20.
//

import UIKit
import AVFoundation

extension UIApplication {
    enum NativeAppType: String {
        case mailTo = "mailto:"
        case phone = "tel://"
    }
    
    typealias ApplicationInfo = (appUrl: URL,webUrl: URL)
    
    class func safeAreaInsets() -> UIEdgeInsets {
        if #available(iOS 11.0, *) {
            let window = (UIApplication.shared.delegate as? AppDelegate)?.window
            let safeAreaInsets = window?.safeAreaInsets ?? .zero
            
            return safeAreaInsets
        }
        else {
            return .zero
        }
    }
    
    class func openNativeApp(valueToAppend: String?, appType: NativeAppType, completion: ((Bool) -> ())? = nil) {
        guard let url = URL(string: appType.rawValue + (valueToAppend ?? "")) else {
            completion?(false)
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
}
