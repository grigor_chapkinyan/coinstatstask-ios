//
//  Constants.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 11/11/20.
//

import Foundation
import CoreGraphics
import UIKit.UIFont
import CoreLocation
import UIKit.UIColor
import LanguageManager_iOS

struct Constants {
    enum AppLanguage: String, CaseIterable {
        case en = "en"
        case arm = "fi"
        
        func getManagerLanguageInstance() -> Languages {
            switch self {
                case .en:
                    return .en
                    
                case .arm:
                    return .fi
            }
        }
        
        func title() -> String {
            switch self {
                case .en:
                    return LabelText.localizedString(.englishUpper)
                    
                case .arm:
                    return LabelText.localizedString(.armenianUpper)
            }
        }
    }
    
    enum UserDefaultsKey: String {
        case currentMainColorRawValue = "currentMainColorRawValue"
        case currentTheme = "currentTheme"
        case defaultLanguage = "defaultLanguage"
    }
    
    enum HttpRequestHeaderName: String {
        case authorization = "Authorization"
        case refreshToken = "refreshtoken"
        case expires = "Expires"
        case bearer = "Bearer"
    }
    
    enum HttpBodyKeys: String {
        case results = "results"
        case page = "page"
        case gender = "gender"
        case name = "name"
        case title = "title"
        case first = "first"
        case last = "last"
        case location = "location"
        case street = "street"
        case number = "number"
        case city = "city"
        case state = "state"
        case country = "country"
        case postcode = "postcode"
        case coordinates = "coordinates"
        case latitude = "latitude"
        case longitude = "longitude"
        case timezone = "timezone"
        case offset = "offset"
        case description = "description"
        case email = "email"
        case login = "login"
        case uuid = "uuid"
        case username = "username"
        case password = "password"
        case salt = "salt"
        case md5 = "md5"
        case sha1 = "sha1"
        case sha256 = "sha256"
        case dob = "dob"
        case date = "date"
        case age = "age"
        case registered = "registered"
        case phone = "phone"
        case cell = "cell"
        case id = "id"
        case value = "value"
        case picture = "picture"
        case large = "large"
        case medium = "medium"
        case thumbnail = "thumbnail"
        case nat = "nat"
    }
    
    enum APIEndpoint: String {
        case `default` = "/api"
        
        static func baseUrl() -> String {
            #if DEBUG
                return "https://randomuser.me"
            #else
                return "https://randomuser.me"
            #endif
        }
    }

    enum StoryboardId: String {
        case main = "Main"
    }
    
    enum ImageName: String {
        case loadingView = "loadingView"
        case usersIcon = "usersIcon"
        case heartIcon = "heartIcon"
        case settingsIcon = "settingsIcon"
        case userHeartNormalIcon = "userHeartNormalIcon"
        case userHeartFilledIcon = "userHeartFilledIcon"
        case maleIcon = "maleIcon"
        case femaleIcon = "femaleIcon"
        case userPlacholder = "userPlacholder"
        case refresh = "refresh"
    }
    
    enum ColorName: String, CaseIterable {
        case generalYellowColor = "generalYellowColor"
        case generalPurpleColor = "generalPurpleColor"
        case generalBlueColor = "generalBlueColor"
        
        func getUIColor() -> UIColor {
            return UIColor(named: self.rawValue)!
        }
    }
    
    enum LabelText: String {
        static func localizedString(_ item: LabelText) -> String {
            return NSLocalizedString(item.rawValue, comment: "").localiz()
        }
        
        case okUpperCase = "Ok"
        case users = "Users"
        case saved = "Saved"
        case settings = "Settings"
        case englishUpper = "English"
        case armenianUpper = "Armenian"
        case lightUpper = "Light"
        case darkUpper = "Dark"
        case autoUpper = "Auto"
        case searchBy = "Search by name, email, phone, location"
        case ageUpper = "Age"
        case backUpper = "Back"
        case genderUpper = "Gender"
        case phoneNumer = "Phone number"
        case addressUpper = "Address"
        case emailUpper = "Email"
        case mainColor = "Main Color"
        case language = "Language"
        case appearance = "Appearance"
        case setDeafult = "Set Default Settings"
    }
        
    // General
    
    static let realmSchemaVersion = 0
    static let tabBarAspectRatioMultiply: CGFloat = 0.14
    static let splashAllAnimationDuration: TimeInterval = 1
    static let transparentBlackViewAlpha: CGFloat = 0.4
    static let disabledViewAlpha: CGFloat = 0.5
    static let requestJsonHeaders = ["Content-Type": "application/json","Accept": "application/json"]
}
