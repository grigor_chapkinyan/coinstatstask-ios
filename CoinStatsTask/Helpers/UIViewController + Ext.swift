//
//  UIViewController + Ext.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 11/11/20.
//

import Foundation
import UIKit

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        
        return instantiateFromNib()
    }
}
