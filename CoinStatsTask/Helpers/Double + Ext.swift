//
//  Double + Ext.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 04.12.20.
//

import Foundation
import CoreGraphics

extension Double {
    func rad() -> CGFloat {
        return CGFloat(self) * .pi / 180
    }
}

