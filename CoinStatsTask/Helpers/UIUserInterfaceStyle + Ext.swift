//
//  UIUserInterfaceStyle + Ext.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 20.07.21.
//

import UIKit

extension UIUserInterfaceStyle: CaseIterable {
    public static var allCases: [UIUserInterfaceStyle] = [.dark, .light, .unspecified]
    
    func getTitle() -> String {
        switch self {
            case .dark:
                return Constants.LabelText.localizedString(.darkUpper)
                
            case .light:
                return Constants.LabelText.localizedString(.lightUpper)
                
            case .unspecified:
                return Constants.LabelText.localizedString(.autoUpper)
                
            @unknown default:
                return ""
        }
    }
}
