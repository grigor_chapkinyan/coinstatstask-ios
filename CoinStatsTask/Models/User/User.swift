//
//  User.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 18.11.20.
//

import Foundation

class User: RealmObjectWithId {
    // MARK: Nested Types
    
    enum Gender: String {
        case male = "male"
        case female = "female"
    }
    
    // MARK: Static Functions
    
    static func==(lhs: User,rhs: User) -> Bool {
        return lhs._id == rhs._id
    }
    
    override static func primaryKey() -> String? {
        return "_id"
    }
    
    // MARK: Properties
    
    @objc dynamic var id: String = ""
    @objc dynamic var genderStr: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var cell: String = ""
    @objc dynamic var nation: String = ""
    @objc dynamic var namePrefix: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var dob: String = ""
    @objc dynamic var age: Int = 0
    @objc dynamic var largeImage: String = ""
    @objc dynamic var mediumImage: String = ""
    @objc dynamic var thumbnailImage: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var state: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var postcode: Int = 0
    @objc dynamic var streetNumber: Int = 0
    @objc dynamic var streetName: String = ""
    @objc dynamic var coordLat: Double = 0
    @objc dynamic var coordLong: Double = 0
    @objc dynamic var timeZoneOffset: String = ""
    @objc dynamic var timeZoneDescription: String = ""

    var gender: Gender? {
        return Gender(rawValue: genderStr)
    }
    
    // MARK: Init

    init?(from dictionary: [String : Any]) {
        guard let idDict = dictionary[Constants.HttpBodyKeys.id.rawValue] as? [String : Any],
              let _ = idDict[Constants.HttpBodyKeys.name.rawValue] as? String,
              let idValue = idDict[Constants.HttpBodyKeys.value.rawValue] as? String,
              let genderStr = dictionary[Constants.HttpBodyKeys.gender.rawValue] as? String,
              let gender = Gender(rawValue: genderStr),
              let email = dictionary[Constants.HttpBodyKeys.email.rawValue] as? String,
              let phone = dictionary[Constants.HttpBodyKeys.phone.rawValue] as? String,
              let cell = dictionary[Constants.HttpBodyKeys.cell.rawValue] as? String,
              let nation = dictionary[Constants.HttpBodyKeys.nat.rawValue] as? String,
              let nameDict = dictionary[Constants.HttpBodyKeys.name.rawValue] as? [String : Any],
              let namePrefix = nameDict[Constants.HttpBodyKeys.title.rawValue] as? String,
              let firstName = nameDict[Constants.HttpBodyKeys.first.rawValue] as? String,
              let lastName = nameDict[Constants.HttpBodyKeys.last.rawValue] as? String,
              let dobDict = dictionary[Constants.HttpBodyKeys.dob.rawValue] as? [String : Any],
              let dob = dobDict[Constants.HttpBodyKeys.date.rawValue] as? String,
              let age = dobDict[Constants.HttpBodyKeys.age.rawValue] as? Int,
              let imageDict = dictionary[Constants.HttpBodyKeys.picture.rawValue] as? [String : Any],
              let largeImage = imageDict[Constants.HttpBodyKeys.large.rawValue] as? String,
              let mediumImage = imageDict[Constants.HttpBodyKeys.medium.rawValue] as? String,
              let thumbnailImage = imageDict[Constants.HttpBodyKeys.thumbnail.rawValue] as? String,
              let locationDict = dictionary[Constants.HttpBodyKeys.location.rawValue] as? [String : Any],
              let streetDict = locationDict[Constants.HttpBodyKeys.street.rawValue] as? [String : Any],
              let coordinatesDict = locationDict[Constants.HttpBodyKeys.coordinates.rawValue] as? [String : Any],
              let timeZoneDict = locationDict[Constants.HttpBodyKeys.timezone.rawValue] as? [String : Any],
              let city = locationDict[Constants.HttpBodyKeys.city.rawValue] as? String,
              let state = locationDict[Constants.HttpBodyKeys.state.rawValue] as? String,
              let country = locationDict[Constants.HttpBodyKeys.country.rawValue] as? String,
              let postcode = locationDict[Constants.HttpBodyKeys.postcode.rawValue] as? Int,
              let streetNumber = streetDict[Constants.HttpBodyKeys.number.rawValue] as? Int,
              let streetName = streetDict[Constants.HttpBodyKeys.name.rawValue] as? String,
              let coordLatStr = coordinatesDict[Constants.HttpBodyKeys.latitude.rawValue] as? String,
              let coordLongStr = coordinatesDict[Constants.HttpBodyKeys.longitude.rawValue] as? String,
              let coordLat = Double(coordLatStr),
              let coordLong = Double(coordLongStr),
              let timeZoneOffset = timeZoneDict[Constants.HttpBodyKeys.offset.rawValue] as? String,
              let timeZoneDescription = timeZoneDict[Constants.HttpBodyKeys.description.rawValue] as? String  else {return nil}
        
        let _id = idValue
        
        super.init()
        
        self.id = idValue
        self._id = _id
        self.genderStr = gender.rawValue
        self.email = email
        self.phone = phone
        self.cell = cell
        self.nation = nation
        self.namePrefix = namePrefix
        self.firstName = firstName
        self.lastName = lastName
        self.dob = dob
        self.age = age
        self.largeImage = largeImage
        self.mediumImage = mediumImage
        self.thumbnailImage = thumbnailImage
        self.city = city
        self.state = state
        self.country = country
        self.postcode = postcode
        self.streetNumber = streetNumber
        self.streetName = streetName
        self.coordLat = coordLat
        self.coordLong = coordLong
        self.timeZoneOffset = timeZoneOffset
        self.timeZoneDescription = timeZoneDescription
    }
    
    required override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
}
