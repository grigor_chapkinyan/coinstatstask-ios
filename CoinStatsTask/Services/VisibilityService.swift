//
//  VisibilityService.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 15.07.21.
//

import UIKit
import RxCocoa
import RxSwift
import LanguageManager_iOS

class VisibilityService {
    // MARK: Input
    
    public let resetSettings = PublishRelay<Void>()
    public let defaultLanguageChange = PublishRelay<Languages>()
    public let mainColorChange = PublishRelay<Constants.ColorName>()
    public let themeChange = PublishRelay<UIUserInterfaceStyle>()

    // MARK: Output
    
    public let currentLanguage = BehaviorRelay<Languages>(value: .en)
    public let currentTheme = BehaviorRelay<UIUserInterfaceStyle>(value: .unspecified)
    public let currentMainColor = BehaviorRelay<Constants.ColorName>(value: .generalYellowColor)
    
    // MARK: Init
    
    static var shared = VisibilityService()
    private init() {
        helperQueue.async { [weak self] in
            self?.setupInitialState()
            self?.setupInitialBindings()
        }
    }
    
    static func setup() {
        shared = VisibilityService()
    }
    
    // MARK: Properties
    
    private let bag = DisposeBag()
    private lazy var helperQueue = DispatchQueue.main
    private lazy var helperScheduler = MainScheduler.asyncInstance
    
    // MARK: Helpers
    
    private func setupInitialState() {
        let storedColor = getStoredMainColor()
        
        if let storedColor = storedColor {
            currentMainColor.accept(storedColor)
        }
        
        let storedTheme = getStoredTheme()
        currentTheme.accept(storedTheme)
        
        let defaultLanguage = getDefaultLanguage()
        self.currentLanguage.accept(defaultLanguage)
    }
    
    private func setupInitialBindings() {
        // Reset
        resetSettings
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] in
                self?.themeChange.accept(.unspecified)
                self?.mainColorChange.accept(.generalYellowColor)
                self?.defaultLanguageChange.accept(.en)
            })
            .disposed(by: bag)
        
        // Color bindings
        mainColorChange
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (color) in
                self?.storeMainColorInfo(colorToSet: color)
            })
            .disposed(by: bag)
        
        currentMainColor
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (color) in
                self?.setupNewColorForGeneralViews(color)
            })
            .disposed(by: bag)
        
        // Theme bindings
        themeChange
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (theme) in
                self?.storeMainThemeInfo(themeToSet: theme)
            })
            .disposed(by: bag)
        
        currentTheme
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (theme) in
                self?.setupNewTheme(theme)
            })
            .disposed(by: bag)
        
        // Language bindings
        defaultLanguageChange
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (language) in
                self?.storeDefaultLanguageInfo(language)
            })
            .disposed(by: bag)
        
        currentLanguage
            .observe(on: helperScheduler)
            .subscribe(onNext: { [weak self] (language) in
                self?.setupNewLanguage(language)
            })
            .disposed(by: bag)
    }
    
    private func storeMainColorInfo(colorToSet: Constants.ColorName) {
        UserDefaults.standard.setValue(colorToSet.rawValue, forKey: Constants.UserDefaultsKey.currentMainColorRawValue.rawValue)
        UserDefaults.standard.synchronize()
        
        currentMainColor.accept(colorToSet)
    }
    
    private func storeMainThemeInfo(themeToSet: UIUserInterfaceStyle) {
        UserDefaults.standard.setValue(themeToSet.rawValue, forKey: Constants.UserDefaultsKey.currentTheme.rawValue)
        UserDefaults.standard.synchronize()
        
        currentTheme.accept(themeToSet)
    }
    
    private func storeDefaultLanguageInfo(_ language: Languages) {
        LanguageManager.shared.setLanguage(language: language)
        
        UserDefaults.standard.setValue(language.rawValue, forKey: Constants.UserDefaultsKey.defaultLanguage.rawValue)
        UserDefaults.standard.synchronize()
        
        currentLanguage.accept(language)
    }
    
    private func getStoredMainColor() -> Constants.ColorName? {
        guard let colorRawValue = UserDefaults.standard.string(forKey: Constants.UserDefaultsKey.currentMainColorRawValue.rawValue) else {
            return nil
        }
        
        let colorToReturn = Constants.ColorName(rawValue: colorRawValue)
        
        return colorToReturn
    }
    
    private func getStoredTheme() -> UIUserInterfaceStyle {
        let themeRawValue = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.currentTheme.rawValue)
        
        let themeToReturn = UIUserInterfaceStyle(rawValue: themeRawValue) ?? .unspecified
        
        return themeToReturn
    }
    
    private func getDefaultLanguage() -> Languages {
        let lastStrValue = UserDefaults.standard.string(forKey: Constants.UserDefaultsKey.defaultLanguage.rawValue) ?? Constants.AppLanguage.en.rawValue
        let lastLanguageSetted: Languages = Constants.AppLanguage(rawValue: lastStrValue)!.getManagerLanguageInstance()
        
        return lastLanguageSetted
    }
    
    private func setupNewColorForGeneralViews(_ color: Constants.ColorName) {
        let colorToSet = color.getUIColor()
        
        (UIApplication.shared.delegate as? AppDelegate)?.window?.tintColor = colorToSet
        UISegmentedControl.appearance().selectedSegmentTintColor = colorToSet
    }
    
    private func setupNewTheme(_ theme: UIUserInterfaceStyle) {
        (UIApplication.shared.delegate as? AppDelegate)?.window?.overrideUserInterfaceStyle = theme
    }
    
    private func setupNewLanguage(_ language: Languages) {
        let window = UIApplication.shared.windows.first
        let instance = LanguageManager.WindowAndTitle(window, nil)

        LanguageManager.shared.setLanguage(language: language, for: [instance], viewControllerFactory: nil, animation: nil)
    }
}
