//
//  DBService.swift
//  CoinStatsTask
//
//  Created by Grigor Chapkinyan on 11/11/20.
//

import Foundation
import RxRealm
import RealmSwift
import RxSwift
import RxCocoa

typealias RealmObjectWithIdArray = (baseType: Object.Type,objects: [RealmObjectWithId])

enum DbServiceError: Error {
    case absentObjectError
}

class RealmObjectWithId: Object, Codable {
    @objc dynamic var _id: String = UUID().uuidString
}

class DBService {
    // MARK: Static Methods
    
    static func setup() {
        shared = DBService()
    }
    
    // MARK: Init
    
    static var shared = DBService()
    private init(){
        helperQueue.async { [weak self] in
            // For TEST printting

            guard let self = self else {return}

            // User
            
            let userCollection = self.realm.objects(User.self)
            
            Observable
                .changeset(from: userCollection)
                .map { $0.0 }
                .subscribe(onNext: { (array) in
                    print("Users count: \(array.count)")
                })
                .disposed(by: self.bag)
        }
    }
    
    // MARK: Private Properties
    
    private let bag = DisposeBag()
    lazy var helperQueue = DispatchQueue(label: "DBService", qos: .default)
    lazy var helperScheduler = SerialDispatchQueueScheduler(queue: helperQueue, internalSerialQueueName: "DBService")
    var realm: Realm {
        autoreleasepool {
            // Configurring Realm
            let realm = try! Realm(configuration: Realm.Configuration.defaultConfiguration, queue: helperQueue)
            
            realm.autorefresh = true
            
            return realm
        }
    }
    
    // MARK: Methods
    
    func refresh() {
        helperQueue.async { [weak self] in
            self?.realm.refresh()
        }
    }
    
    func writeObjects<T: Object>(objects: [T]) -> Observable<Void> {
        return Observable.create { [weak self] (observer) -> Disposable in
            do {
                try self?.realm.write({ () in
                    self?.realm.add(objects, update: .modified)
                })
            }
            catch(let error) {
                print("DB adding error: \(error.localizedDescription)")
            }
            
            self?.realm.refresh()
            
            self?.cleanOddChildrenObjects()
            
            self?.realm.refresh()
            
            observer.onNext(())
            observer.onCompleted()
            
            return Disposables.create()
        }
        .subscribe(on: helperScheduler)
        .take(1)
    }
    
    func remove(baseType: Object.Type = RealmObjectWithId.self, predicate: NSPredicate) -> Observable<Void> {
        return Observable.create { [weak self] (observer) -> Disposable in
            guard let allObjects = self?.realm.objects(baseType.self) else {
                observer.onError(DbServiceError.absentObjectError)
                return Disposables.create()
            }
            
            let objectsToRemove = allObjects.filter(predicate).toArray()

            do {
                try self?.realm.write { () in
                    self?.realm.cascadingDelete(objectsToRemove)
                }
            }
            catch(let error) {
                print("DB deleting error: \(error.localizedDescription)")
                observer.onError(error)
            }
            
            self?.realm.refresh()
                        
            observer.onNext(())
            observer.onCompleted()
            
            return Disposables.create()
        }
        .subscribe(on: helperScheduler)
        .take(1)
    }
    
    func updateWith<T: RealmObjectWithId>(newObjects: [T],baseType: Object.Type = RealmObjectWithId.self,mustCleanOddObjects: Bool = true) -> Observable<Void> {
        return Observable.create { [weak self] (observer) -> Disposable in
            guard let allObjects = self?.realm.objects(baseType.self) else {
                observer.onError(DbServiceError.absentObjectError)
                return Disposables.create()
            }
            
            let oddObjects: [RealmSwift.Object] = allObjects.filter({ (currentOldObject) -> Bool in
                guard let currentOldObject = currentOldObject as? RealmObjectWithId else {return true}

                guard !currentOldObject.isInvalidated else {return false}

                return !newObjects.contains(where: { $0._id == currentOldObject._id })
            })
            
            do {
                try self?.realm.write { () in
                    if (mustCleanOddObjects) {
                        // Deleting Odd objects
                        self?.realm.cascadingDelete(oddObjects)
                    }
                    
                    // Adding new objects
                    self?.realm.add(newObjects, update: .modified)
                }
            }
            catch(let error) {
                print("DB deleting error: \(error.localizedDescription)")
                observer.onError(error)
            }
            
            self?.realm.refresh()
            
            self?.cleanOddChildrenObjects()
            
            self?.realm.refresh()
            
            observer.onNext(())
            observer.onCompleted()
            
            return Disposables.create()
        }
        .subscribe(on: helperScheduler)
        .take(1)
    }
    
    func remove<T: Object>(objects: [T],cleanChildObjects: Bool = true) -> Observable<Void> {
        return Observable.create { [weak self] (observer) -> Disposable in
            do {
                try self?.realm.write { () in
                    let validObjects = objects.filter({ !$0.isInvalidated })
                    
                    self?.realm.cascadingDelete(validObjects)
                }
            }
            catch(let error) {
                print("DB deleting error: \(error.localizedDescription)")
                observer.onError(error)
            }
            
            self?.realm.refresh()
            
            if (cleanChildObjects) {
                self?.cleanOddChildrenObjects()

                self?.realm.refresh()
            }
            
            observer.onNext(())
            observer.onCompleted()
            
            return Disposables.create()
        }
        .subscribe(on: helperScheduler)
        .take(1)
    }
    
    func removeAll() -> Observable<Void> {
        return Observable.create { [weak self] (observer) -> Disposable in
            do {
                try self?.realm.write { () in
                    self?.realm.deleteAll()
                }
            }
            catch(let error) {
                print("DB deleting error: \(error.localizedDescription)")
                observer.onError(error)
            }
            
            self?.realm.refresh()
            
            observer.onNext(())
            observer.onCompleted()
            
            return Disposables.create()
        }
        .subscribe(on: helperScheduler)
        .take(1)
    }
    
    // MARK: Helpers
    
    private func cleanOddChildrenObjects() { }
}
